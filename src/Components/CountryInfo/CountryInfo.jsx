import React, { useContext } from "react";
import "./CountryInfo.css";
import { Link, useParams } from "react-router-dom";
import { ThemeContext } from "../../App";
function CountryInfo({ countryList }) {
  const id = useParams().id;

  const item = countryList.filter((data) => {
    return data.cca3 == id;
  });
  // console.log(item);
  const [darkTheme, switchDarkTheme] = useContext(ThemeContext);
  return (
    <div>
      <div className={darkTheme ? 'nav-bar2 dark-clr1' : 'nav-bar2'}>
        <Link to={"/"}>
          <button id="back-button" className={darkTheme ? 'dropdown dark-clr2' : 'dropdown'}>
            <i className="fa-solid fa-arrow-left"></i> Back
          </button>
        </Link>
      </div>
      <div className={darkTheme ? 'country-info all-country-data dark-clr2' : 'country-info all-country-data white-clr'} >
        <img
          className="country-image-solo country-image"
          src={item[0].flags.svg}
          alt="Image Of Country"
        />
        <div className="all-detail">
          <h2 className="country-name">{item[0].name.common}</h2>
          <div className="country-details-solo solo-country">
            <div>
              <p>
                <span className="highlight">Native Name:</span>{" "}
                {console.log('native nanme',Object.values(item[0].name.nativeName)[0].common)}
                <span>{item[0].currencies == undefined ? (
                  <span>NA </span>
                ) : Object.values(item[0].name.nativeName)[0].common}</span>
              </p>
              <p>
                <span className="highlight">Population:</span>{" "}
                <span>{item[0].population}</span>
              </p>
              <p>
                <span className="highlight">Region:</span>{" "}
                <span>{item[0].region}</span>
              </p>
              <p>
                <span className="highlight">Sub Region:</span>{" "}
                {item[0].subregion == undefined ? (
                  <span>No Subregion </span>
                ) : (
                  <span>{item[0].subregion}</span>
                )}
              </p>
              <p>
                <span className="highlight">Capitals:</span>{" "}
                {item[0].capital == undefined ? (
                  <span>No Capitals </span>
                ) : (
                  <span>{item[0].capital}</span>
                )}
              </p>
            </div>
            <div>
              <p>
                <span className="highlight">Top Level Domain :</span>{" "}
                {item[0].tld[0] == undefined ? (
                  <span>No Top level domain </span>
                ) : (
                  <span>{item[0].tld[0]}</span>
                )}
              </p>
              <p>
                <span className="highlight">Currencies:</span>{" "}
                {item[0].currencies == undefined ? (
                  <span>No Currency </span>
                ) : (
                  <span>{Object.keys(item[0].currencies)[0]} </span>
                )}
              </p>
              <p>
                <span className="highlight">Languages:</span>{" "}
                {item[0].languages == undefined ? (
                  <span>No Language </span>
                ) : (
                  Object.values(item[0].languages).map((lang) => {
                    return <span>{lang} </span>;
                  })
                )}
              </p>
            </div>
          </div>
          <p>
            <span className="highlight">Border Countries:</span>

            {item[0].borders == undefined ? (
              <span>No Border</span>
            ) : (
              item[0].borders.map((country) => {
                return (
                  <Link to={`/country/${country}`}>
                    
                    <button className="change-country change-mode">
                      
                      {
                        countryList.find((data) => {
                          return data.cca3 == country;
                        }).name.common
                      }
                    </button>
                  </Link>
                );
              })
            )}
          </p>
        </div>
      </div>
    </div>
  );
}

export default CountryInfo;
