import React, { useContext } from 'react'
import './allCountryData.css'
import { Link} from 'react-router-dom'
import { ThemeContext } from '../../App';
function AllCountryData({countryList}) {
  // console.log(countryList);
  const [darkTheme, switchDarkTheme] = useContext(ThemeContext);
  return (
    <div className={darkTheme ? 'all-country-data dark-clr2' : 'all-country-data white-clr'} >
        {
        countryList.length == 0 ? <h1>Reselect the region or Subregion Data</h1>:
        countryList.map((item)=>{
            return  <div className={darkTheme ? 'country-data dark-clr2' : 'country-data white-clr'}>
                <Link to={`/country/${item.cca3}`}><img className='country-image' src={item.flags.png} alt=""/>  </Link>
                <div className='country-details'>
                    <h2 className='country-name'>{item.name.common}</h2>
                    <p><span className='highlight'>Population:</span> <span>{item.population}</span></p>
                    <p><span className='highlight'>Region:</span> <span>{item.region}</span></p>
                    <p><span className='highlight'>Capital:</span> <span>{item.capital == undefined ?'No Capital' :item.capital  }</span></p>
                </div>
            </div>
       })
    }
       
    </div>
  )
}

export default AllCountryData