import React, { useContext } from "react";
import './navBar.css';
import { ThemeContext } from "../../App";

function NavBar({ setRegion,region, countryName, checkCountry, setSort, subregionList, setSubRegionList, setSubRegion,}) {
  
  const [darkTheme] = useContext(ThemeContext);
  
  return (
      <div className={darkTheme ? 'nav-bar2 dark-clr1' : 'nav-bar2'} >
        <input
          type="text"
          id="search"
          className= {darkTheme ? 'dropdown dark-clr2' : 'dropdown'}
          placeholder="search for country"
          value={countryName}
          onChange={(event) => {
            checkCountry(event.target.value);
          }}
        />
        <div>
        <select
            onChange={(event) => {
              setSort(event.target.value);
            }}
            className= {darkTheme ? 'dropdown dark-clr2' : 'dropdown'}
          >
            <option value="">Sort By </option>
            <option value="ascending-population"> Ascending Population</option>
            <option value="descending-population">Descending Population</option>
            <option value="ascending-area">Ascending Area</option>
            <option value="descending-area">Descending Area</option>
          </select>

          <select
            onChange={(event) => {
              setSubRegion(event.target.value);
              console.log(event.target.value);
            }}
            className= {darkTheme ? 'dropdown dark-clr2' : 'dropdown'}
          >
            <option value="">Select By Subregion</option>
            {
                subregionList.map((item)=>{
                  return <option key={item} value={item}>{item}</option>
                })
            }
          </select>

          <select
            onChange={(event) => {
              setRegion(event.target.value);
              setSubRegion([])
              if(region == ''){
                setSubRegionList([]);
              }
            }}
            className= {darkTheme ? 'dropdown dark-clr2' : 'dropdown'}
          >
            <option value="">Select By Region</option>
            <option value="Oceania">Oceania</option>
            <option value="Americas">Americas</option>
            <option value="Asia">Asia</option>
            <option value="Europe">Europe</option>
            <option value="Africa">Africa</option>
            <option value="Antarctic">Antarctic</option>
          </select>
        </div>
      </div>
  );
}

export default NavBar;
