import React, { useState, useEffect, useContext } from "react";
import './headBar.css';
import { ThemeContext } from "../../App";

function HeadBar() {
  const [buttonName, setButtonName] = useState(
    <h2>
      <i className="fa-solid fa-moon"></i> Dark mode
    </h2>
  );
  const [darkTheme, switchDarkTheme] = useContext(ThemeContext);
  return (
        <div className={darkTheme ? 'nav-bar1 dark-clr2' : 'nav-bar1 white-clr'}>
        <h1>Where in the world</h1>
        <div>
          <button
          className={darkTheme ? 'dark-clr2' : ''}
          onClick={()=>{
            if(darkTheme){
              switchDarkTheme(false);
              setButtonName(
                        <h2>
                          <i className="fa-solid fa-moon"></i> Dark mode
                        </h2>
                      )
            }else{
              switchDarkTheme(true);
              setButtonName(
                        <h2>
                          <i className="fa-regular fa-moon"></i> Light Mode
                        </h2>
                      )
            }
          }}
          >
            {buttonName}
          </button>
        </div>
      </div>
  )
}

export default HeadBar