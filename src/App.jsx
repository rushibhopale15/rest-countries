import "./App.css";
// import RestCountry from "./Components/Rest-country";
import React, { useEffect, useState } from "react";
import HeadBar from "./Components/HeadBar/HeadBar";
import NavBar from "./Components/NavBar/NavBar";
import AllCountryData from "./Components/AllCountryData/AllCountryData";
import Loader from "./Components/Loader/Loader";
import CountryInfo from "./Components/CountryInfo/CountryInfo";
import { Route,Routes } from "react-router-dom";

export const ThemeContext = React.createContext();
function App() {
  const [countryList, setCountryList] = useState([]);
  const [region, setRegion] = useState("");
  const [countryName, checkCountry] = useState("");
  const [sortData, setSort] = useState("");
  const [subregionList, setSubRegionList] = useState([]);
  const [subRegion, setSubRegion] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [errorMessage, setError] = useState('');

  const [darkTheme, switchDarkTheme] = useState(false);
  useEffect(() => {
    fetch("https://restcountries.com/v3.1/all")
      .then((data) => data.json())
      .then((data) => {
        setCountryList(data);
        setIsLoading(false);
      })
      .catch((error) => {
        console.error(error.message);
        // setError('data not found')
        setError(error.message)
      });
  },[]);

  function findRegionData(){
    const regionData = countryList.filter((item) => {
      return item.region.includes(region);
    });
    return regionData;
  }
  const regionData = findRegionData();
  
//find data in key value pair subregion : country
let subRegionDataList = regionData.reduce((accumulator, item) => {
  let subRegion = item.subregion
  if(subRegion == undefined){
    subRegion = 'Antarctic';
  }
  if (subRegion in accumulator) {
    accumulator[subRegion].push(item);
  } else {
    accumulator[subRegion] = [item];
  }
  return accumulator;
}, {});

//when region change subRegion data sets
useEffect(()=>{
  setSubRegionList(Object.keys(subRegionDataList));
  console.log('subRegions', subregionList);
},[region])


  const dataSubRegionList = regionData.filter((item)=>{ 
    if(item.subregion == undefined){
      return item.region.includes('Antarctic');
    }
    else{
    return item.subregion.includes(subRegion);
    }
  // console.log(item.subregion ==subregion);
  })
  const searchData = dataSubRegionList.filter((item) => {
    return item.name.common.toLowerCase().includes(countryName);
  });


  if (sortData == "ascending-population") {
    searchData.sort((item1, item2) => {
      return item1.population - item2.population;
    });
  }
  if (sortData == "descending-population") {
    searchData.sort((item1, item2) => {
      return item2.population - item1.population;
    });
  }
  if (sortData == "ascending-area") {
    searchData.sort((item1, item2) => {
      return item1.area - item2.area;
    });
  }
  if (sortData == "descending-area") {
    searchData.sort((item1, item2) => {
      return item2.area - item1.area;
    });
  }

  if(errorMessage){
    return <h1>{errorMessage}</h1>;
  }
 
  return (
    <>
    {isLoading ? <Loader /> :
        <Routes>
       <Route path="/" element={<>
          <ThemeContext.Provider value={[darkTheme, switchDarkTheme]}>
           <HeadBar/>
            <NavBar
                setRegion={setRegion}
                region ={region}
                countryName={countryName}
                checkCountry={checkCountry}
                setSort={setSort}
                subregionList={subregionList}
                setSubRegionList={setSubRegionList}
                setSubRegion = {setSubRegion}
            />
            <AllCountryData countryList={searchData} /> 
          </ThemeContext.Provider>
            </>}></Route>

         <Route path="/country/:id" element={<>
          <ThemeContext.Provider value={[darkTheme, switchDarkTheme]}>
            <HeadBar/>
            <CountryInfo countryList={countryList}/>
          </ThemeContext.Provider>
         </>}></Route>
         </Routes>
    }
    </>
  );

}

export default App;
